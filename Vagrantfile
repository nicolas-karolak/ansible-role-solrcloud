# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider "virtualbox" do |v|
    v.cpus = 1
    v.memory = 256
    v.check_guest_additions = false
  end

  config.vm.define "lb" do |lb|
    lb.vm.hostname = "lb"
    lb.vm.network "private_network", "ip": "192.168.123.10"
  end

  NB_NODES = 5
  NODES_NAMES = []
  NODES_IPS = []
  (1..NB_NODES).each do |i|
    name = "solr-#{i}"
    NODES_NAMES.push(name)
    ip = "192.168.123.1#{i}"
    NODES_IPS.push(ip)
    config.vm.define name do |solr|
      solr.vm.hostname = name
      solr.vm.network "private_network", ip: ip
      if i == NB_NODES
        solr.vm.provision "ansible" do |ansible|
          #ansible.verbose = "vvv"
          ansible.become = true
          ansible.groups = {
            "solrcloud_server" => NODES_NAMES,
            "loadbalancer_server" => ["lb"],
          }
          ansible.limit = "all"
          ansible.playbook = "site.yml"
          ansible.extra_vars = {
            "zk_id": "{% for node in groups['solrcloud_server'] %}{% if node == inventory_hostname %}{{ loop.index }}{% endif %}{% endfor%}",
            "zk_config": {
              "clientPort": "2181",
              "tickTime": "2000",
              "dataDir": "{{ zk_home }}",
              "4lw.commands.whitelist": "mntr,conf,ruok",
              "initLimit": "5",
              "syncLimit": "2",
              "autopurge.snapRetainCount": "3",
              "autopurge.purgeInterval": "1",
              "jute.maxbuffer": "0x63fffff",
              "admin.enableServer": "false",
            },
            "solr_config": {
              "solr_install_dir": "{{ solr_install_path }}",
              "runas": "{{ solr_user_name }}",
              "solr_pid_dir": "{{ solr_user_home }}",
              "solr_home": "{{ solr_user_home }}/data",
              "log4j_props": "{{ solr_user_home }}/log4j2.xml",
              "solr_logs_dir": "{{ solr_user_home }}/logs",
              "solr_port": "{{ solr_port }}",
              "solr_host": "{{ ansible_eth1.ipv4.address }}",
              "zk_host": "{% for node in groups['solrcloud_server'] %}{{ hostvars[node]['ansible_eth1']['ipv4']['address'] }}:2181{% if not loop.last %},{% endif %}{% endfor%}",
            },
          }
          (1..NB_NODES).each do |j|
            ansible.extra_vars[:zk_config].update("server.#{j}": "{{ hostvars['solr-#{j}']['ansible_eth1']['ipv4']['address'] }}:2888:3888")
          end
        end
      end
    end
  end
end
