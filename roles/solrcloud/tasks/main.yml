---

- name: "install requirements"
  apt:
    install_recommends: false
    cache_valid_time: 86400
    name: "{{ solr_requirements }}"

- name: "create group"
  group:
    name: "{{ solr_user_group }}"
    system: true

- name: "create user"
  user:
    group: "{{ solr_user_group }}"
    name: "{{ solr_user_name }}"
    home: "{{ solr_user_home }}"
    comment: "Solr"
    system: true
    password: "!"
    shell: "/usr/sbin/nologin"
    skeleton: ""

- name: "set sysctl"
  loop: "{{ solr_sysctl }}"
  ansible.posix.sysctl:
    sysctl_file: "/etc/sysctl.d/solr.conf"
    name: "{{ item.name }}"
    value: "{{ item.value }}"

- name: "download archive"
  register: "solr_download"
  get_url:
    url: "{{ solr_dl_url }}"
    checksum: "{{ solr_dl_checksum }}"
    dest: "{{ solr_dl_dest }}"

- name: "extract archive"
  when: "solr_download is changed"
  notify: "restart solr"
  unarchive:
    remote_src: true
    src: "{{ solr_dl_dest }}"
    dest: "/opt/"
    owner: "root"
    group: "root"

- name: "create symlink"
  file:
    src: "{{ solr_install_path }}-{{ solr_version }}"
    dest: "{{ solr_install_path }}"
    state: "link"

- name: "create service"
  notify: "restart solr"
  template:
    src: "solr.service.j2"
    dest: "/etc/systemd/system/solr.service"

- name: "create configuration"
  notify: "restart solr"
  template:
    src: "solr.in.sh.j2"
    dest: "/etc/default/solr.in.sh"

- name: "create data directory"
  file:
    path: "{{ solr_config.solr_home }}"
    group: "{{ solr_user_group }}"
    owner: "{{ solr_user_name }}"
    mode: "750"
    state: "directory"

- name: "create logs directory"
  file:
    path: "{{ solr_config.solr_logs_dir }}"
    group: "{{ solr_user_group }}"
    owner: "{{ solr_user_name }}"
    mode: "750"
    state: "directory"

- name: "create data configuration"
  notify: "restart solr"
  copy:
    src: "solr.xml"
    dest: "{{ solr_config.solr_home }}/solr.xml"
    group: "{{ solr_user_group }}"
    owner: "{{ solr_user_name }}"
    mode: "640"

- name: "create logs configuration"
  notify: "restart solr"
  copy:
    src: "log4j2.xml"
    dest: "{{ solr_user_home }}/log4j2.xml"
    group: "{{ solr_user_group }}"
    owner: "{{ solr_user_name }}"
    mode: "640"

- name: "enable and start service"
  systemd:
    name: "solr.service"
    enabled: true
    state: "started"
