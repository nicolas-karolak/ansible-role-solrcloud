#!/bin/bash

set -Eeuo pipefail

collection=${1}

export SOLR_URL="http://192.168.123.10:8983/solr/${collection}/update"

for i in $(seq 3); do
  python3 indexer.py out.json t${i}_ >/dev/null &
done

wait
